#!/bin/bash

luck=$(xmodmap -pk | grep KP_Enter)

if [ -n "$luck" ]; then
    xmodmap -e "keycode 104 = Tab"
    notify-send "Tab" "The KeyPad Enter key will now act as a Tab key."
else
    xmodmap -e "keycode 104 = KP_Enter"
    notify-send "Enter" "The KeyPad Enter key has returned to being an Enter key."
fi

