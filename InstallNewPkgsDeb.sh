#!/bin/bash

# Install common packages if not already installed
#	Note: this file must be run with sudo privilages to work

#Program install list
# mpd: a music database which many programs use, including my favorite
# cantata: My favorite music player
# zoom: The virtual meeting program
# libreoffice: my go-to suite of office apps--I use the writer program to
#	write all my novels
# thunderbird: My main email client
# nano: my favorite command-line text editor
# celluloid: My favorite video player
# mc: Midnight Commander--a powerful command-line filemanager
# obs: OBS Studio--a versitial app for broadcasting to YouTube and many other services.
# simplescreenrecorder: a lightweight screen recorder and audio capture for my more resource-challenged laptop
# krita: Krita--a very good graphical image editor
# neofetch: Used to display system info 
# htop: Used to display live system info and act as a task manager from the commandline
# curl: installed mainly for downloading Brave browser
# flatpak: Flatpak--used to install some programs not in the repositories, specifically zoom and celluloid.

plist="mpd cantata filezilla libreoffice zoom nano celluloid mc krita inkscape neofetch htop curl flatpak firefox "
 
# List of programs you may want later, but not right now:
# simplescreenrecorder gnupg

# fonts to consider installing: ubuntu-restricted-extras ubuntustudio-fonts ttf-sjfonts ttf-mscorefonts-installer

# Install the list of programs if not already installed.
echo "Programs listed have already been installed ..."

for I in $plist
do

	which $I
	if [ $? -gt 0 ]; then
		if [ $I == "obs" ]; then
			I="obs-studio"
		fi
		echo "Installing $I . . ." 
		sudo apt-get -y -qq install $I
	else
        	echo "Program $I is already installed." 
	fi

done

# Uncomment the following lines if you wish to install the ssh server

#ls /etc/ssh/sshd_config
#if [ $? -gt 0 ]; then
#	echo "Installing OpenSSH Server . . ." 
#  	sudo apt-get -y -qq install openssh-server
#else
#    echo "Program OpenSSH Server is already installed." 
#fi

#Install Brave Broswer if not already installed

#which brave-browser

#if [ $? -gt 0 ]; then
#	echo "Installing the Brave Browser . . ." 
#	sudo apt-get -y -qq install apt-transport-https
#	curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -    ### Depreciated apt-key
	
	# curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | gpg --no-default-keyring --keyring gnupg-ring:/etc/apt/trusted.gpg.d/brave-browser-release.gpg --import -  ### New format when Brave updates their key file

#	sudo echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | tee /etc/apt/sources.list.d/brave-browser-release.list

#	sudo apt-get -y -qq update

#	sudo apt-get -y -qq install brave-browser

#else
#    echo "Program Brave Browser is already installed." 
#fi

# Install crimson-text font

cd $HOME/Downloads

ls /usr/share/fonts/truetype | grep Crimson
if [ $? -gt 0 ]; then
	echo "Installing the Crimson Text font . . ." 
	curl -O https://www.1001freefonts.com/d/5825/crimson-text.zip
	sudo unzip -d /usr/share/fonts/truetype /home/rick/Downloads/crimson-text.zip
	rm /home/rick/Downloads/crimson-text.zip
else
    echo "Font Crimson Text is already installed." 
fi

cd $HOME

# Install candy icon set - This tarball can be downloaded at:
#	https://www.gnome-look.org/p/1305251/
#	Click on the "Download" button on the right-top of the window
#	then click on the tar file to download it.

if [ -f $HOME/Downloads/candy-icons/candy-icons.tar.xz ]; then

cd $HOME/Downloads/candy-icons
ls $HOME/.icons
if [ $? -gt 0 ]; then
	echo "Installing Candy Icons . . ."
	if [ ! -d $HOME/.icons ]; then
		mkdir $HOME/.icons
	fi
	tar -xf ./candy-icons.tar.xz --one-top-level=$HOME/.icons
else
	echo "Candy Icons alreay installed."
fi

cd $HOME

else
	echo "Skipping installing candy icons due to can't find file."
	sleep 2
fi

# Install Celluloid via Flatpak if not installed above

which celluloid
if [ $? -gt 0 ]; then
    flatpak list | grep celluloid
    if [ $? -gt 0 ]; then       
    	echo "Installing Celluloid via Flatpaks . . ." 
    	flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

    	flatpak --noninteractive install flathub io.github.celluloid_player.Celluloid
    else
        echo "Program Celluloid is already installed via Flatpak." 
    fi
else
    echo "Program Celluloid is already installed." 
fi

exit

# Going to manually download and install zoom program from zoom.us website, due to the
#	flatpak version being somewhat buggy. But keeping this in case I want to use it someday.

which zoom
if [ $? -gt 0 ]; then
    flatpak list | grep us.zoom.Zoom
    if [ $? -gt 0 ]; then
        echo "Installing Zoom via Flatpak . . ." 
    	flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

    	flatpak --noninteractive install flathub us.zoom.Zoom
    else
        echo "Program Zoom is already installed via Flatpak." 
    fi
else
    echo "Program Zoom is already installed." 
fi


exit

