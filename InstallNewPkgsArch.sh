#!/bin/bash

# Install common packages if not already installed
#	Note: this file must be run with sudo privilages to work

#Program install list
# mpd: a music database which many programs use, including my favorite
# cantata: My favorite music player
# zoom: The virtual meeting program
# libreoffice: my go-to suite of office apps--I use the writer program to
#	write all my novels
# thunderbird: My main email client
# nano: my favorite command-line text editor
# celluloid: My favorite video player
# mc: Midnight Commander--a powerful command-line filemanager
# obs: OBS Studio--a versitial app for broadcasting to YouTube and many other services.
# simplescreenrecorder: a lightweight screen recorder and audio capture for my more resource-challenged laptop
# krita: Krita--a very good graphical image editor
# neofetch: Used to display system info 
# htop: Used to display live system info and act as a task manager from the commandline
# curl: installed mainly for downloading Brave browser
# flatpak: Flatpak--used to install some programs not in the repositories, specifically zoom and celluloid.

plist="mpd cantata thunderbird libreoffice inkscape zoom obs nano celluloid mc krita neofetch htop curl flatpak firefox"

# Packages on hold: simplescreenrecorder gnupg
# fonts to consider installing: ubuntu-restricted-extras ubuntustudio-fonts ttf-sjfonts ttf-mscorefonts-installer

# Install the list of programs if not already installed.
echo "Programs listed have already been installed ..."

for I in $plist
do

	which $I
	if [ $? -gt 0 ]; then
		if [ $I == "obs" ]; then
			I="obs-studio"
		fi
		echo "Installing $I . . ." 
		pacman -S --noconfirm $I
	else
        	echo "Program $I is already installed." 
	fi

done

# Uncomment the following lines if you wish to install the ssh server

#ls /etc/ssh/sshd_config
#if [ $? -gt 0 ]; then
#	echo "Installing OpenSSH Server . . ." 
#  	pacman -S --noconfirm openssh-server
#else
#    echo "Program OpenSSH Server is already installed." 
#fi

# Install crimson-text font

cd /home/rick/Downloads

ls /usr/share/fonts/TTF | grep Crimson
if [ $? -gt 0 ]; then
	echo "Installing the Crimson Text font . . ." 
	curl -O https://www.1001freefonts.com/d/5825/crimson-text.zip
	unzip -d /usr/share/fonts/TTF /home/rick/Downloads/crimson-text.zip
	rm /home/rick/Downloads/crimson-text.zip
else
    echo "Font Crimson Text is already installed." 
fi

cd /home/rick

# Install Celluloid via Flatpak if not installed above

which celluloid
if [ $? -gt 0 ]; then
    flatpak list | grep celluloid
    if [ $? -gt 0 ]; then       
    	echo "Installing Celluloid via Flatpaks . . ." 
    	flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

    	flatpak --noninteractive install flathub io.github.celluloid_player.Celluloid
    else
        echo "Program Celluloid is already installed via Flatpak." 
    fi
else
    echo "Program Celluloid is already installed." 
fi

exit

