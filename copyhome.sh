
#!/bin/bash

# This is a bash file to backup my home directory onto the SSD card.
# Put any files or file pattterns to exclude in copyhome.info

# Check if backup log file has been created or not, and create if not.
bklog='/home/rick/bkuplogs/BackUp.log'

if [ ! -f /home/rick/bkuplogs/BackUp.log ]; then
	if [ ! -d ~/bkuplogs ]; then
		mkdir ~/bkuplogs
	fi
	touch ~/bkuplogs/BackUp.log
fi

# Begin copying the home directory files to SSD card

Dest='/media/rick/258G SSD'

	printf "\nProcessing . . . Stand By"
	printf "\n-----------------------------------\n Backup changed files on $(date) \n" | tee -a "/home/rick/bkuplogs/BackUp.log"
	rsync -avW --delete-during /home/rick/.bash_aliases "$Dest" | tee -a "/home/rick/bkuplogs/BackUp.log"
	rsync -avW --delete-during /home/rick/.config/obs-studio "$Dest/.config" | tee -a "/home/rick/bkuplogs/BackUp.log"
	rsync -avW --delete-during /home/rick/.mozilla "$Dest" | tee -a "/home/rick/bkuplogs/BackUp.log"
	rsync -avW --delete-during /home/rick/.thunderbird "$Dest" | tee -a "/home/rick/bkuplogs/BackUp.log"
	rsync -avW --delete-during --exclude-from='/home/rick/bin/copyhome.info' /home/rick/bin "$Dest" | tee -a "/home/rick/bkuplogs/BackUp.log"
	rsync -avW --delete-during --exclude-from='/home/rick/bin/copyhome.info' /home/rick/Documents "$Dest" | tee -a "/home/rick/bkuplogs/BackUp.log"
	rsync -avW --delete-during --exclude-from='/home/rick/bin/copyhome.info' /home/rick/Downloads "$Dest" | tee -a "/home/rick/bkuplogs/BackUp.log"
	rsync -avW --delete-during --exclude-from='/home/rick/bin/copyhome.info' /home/rick/Music "$Dest" | tee -a "/home/rick/bkuplogs/BackUp.log"
	rsync -avW --delete-during --exclude-from='/home/rick/bin/copyhome.info' /home/rick/Videos "$Dest" | tee -a "/home/rick/bkuplogs/BackUp.log"
	rsync -avW --delete-during --exclude-from='/home/rick/bin/copyhome.info' /home/rick/Pictures "$Dest" | tee -a "/home/rick/bkuplogs/BackUp.log"
	rsync -avW --delete-during --exclude-from='/home/rick/bin/copyhome.info' /home/rick/PDF "$Dest" | tee -a "/home/rick/bkuplogs/BackUp.log"
	rsync -avW --delete-during --exclude-from='/home/rick/bin/copyhome.info' /home/rick/websites "$Dest" | tee -a "/home/rick/bkuplogs/BackUp.log"
	printf "\nFinished!\n"  | tee -a "/home/rick/bkuplogs/BackUp.log"
exit
