#!/bin/bash
export DISPLAY
export XAUTHORITY

# Isolate line showing whether wifi is enabled
# or not and save in varible
var1=$(export DISPLAY; export XAUTHORITY; iwconfig |  grep -o "ESSID:off")

# Toggle wifi based on var1 varible
if [ "$var1" = "" ];then
	nmcli radio wifi off
else
	nmcli radio wifi on
fi
exit 