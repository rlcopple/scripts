#!/bin/bash

# Install common packages if not already installed
#	Note: this file must be run with sudo privilages to work

#Program install list
# mpd: a music database which many programs use, including my favorite
# cantata: My favorite music player
# zoom: The virtual meeting program
# libreoffice: my go-to suite of office apps--I use the writer program to
#	write all my novels
# thunderbird: My main email client
# nano: my favorite command-line text editor
# celluloid: My favorite video player
# mc: Midnight Commander--a powerful command-line filemanager
# obs: OBS Studio--a versitial app for broadcasting to YouTube and many other services.
# simplescreenrecorder: a lightweight screen recorder and audio capture for my more resource-challenged laptop
# krita: Krita--a very good graphical image editor
# neofetch: Used to display system info 
# htop: Used to display live system info and act as a task manager from the commandline
# curl: installed mainly for downloading Brave browser
# flatpak: Flatpak--used to install some programs not in the repositories, specifically zoom and celluloid.

plist="mpd cantata libreoffice thunderbird inkscape nano celluloid mc simplescreenrecorder krita neofetch htop curl flatpak firefox"

# fonts to consider installing: ubuntu-restricted-extras ubuntustudio-fonts ttf-sjfonts ttf-mscorefonts-installer

# Install the list of programs if not already installed.
echo "Programs listed have already been installed ..."

for I in $plist
do

	which $I
	if [ $? -gt 0 ]; then
		if [ $I == "obs" ]; then
			I="obs-studio"
		fi
		echo "Installing $I . . ." 
		dnf -y install $I
	else
        	echo "Program $I is already installed." 
	fi

done

# Uncomment the following lines if you wish to install the ssh server

#ls /etc/ssh/sshd_config
#if [ $? -gt 0 ]; then
#	echo "Installing OpenSSH Server . . ." 
#  	apt-get -y -qq install openssh-server
#else
#    echo "Program OpenSSH Server is already installed." 
#fi

#Install Brave Broswer if not already installed

which brave-browser

if [ $? -gt 0 ]; then
	echo "Installing the Brave Browser . . ." 
	dnf -y install dnf-plugins-core
	dnf -y config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/x86_64/
	rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc
	dnf -y install brave-browser
else
    echo "Program Brave Browser is already installed." 
fi

# Install crimson-text font

cd /home/rick/Downloads

ls  /usr/share/fonts/Crimson | grep Crimson
if [ $? -gt 0 ]; then
	echo "Installing the Crimson Text font . . ." 
	curl -O https://www.1001freefonts.com/d/5825/crimson-text.zip
	mkdir /usr/share/fonts/Crimson
	unzip -d /usr/share/fonts/Crimson /home/rick/Downloads/crimson-text.zip
	rm /home/rick/Downloads/crimson-text.zip
else
    echo "Font Crimson Text is already installed." 
fi

cd /home/rick

# Install Celluloid via Flatpak if not installed above

which celluloid
if [ $? -gt 0 ]; then
    flatpak list | grep celluloid
    if [ $? -gt 0 ]; then       
    	echo "Installing Celluloid via Flatpaks . . ." 
    	flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

    	flatpak --noninteractive install flathub io.github.celluloid_player.Celluloid
    else
        echo "Program Celluloid is already installed via Flatpak." 
    fi
else
    echo "Program Celluloid is already installed." 
fi

exit

# Going to manually download and install zoom program from zoom.us website, due to the
#	flatpak version being somewhat buggy. But keeping this in case I want to use it someday.

which zoom
if [ $? -gt 0 ]; then
    flatpak list | grep us.zoom.Zoom
    if [ $? -gt 0 ]; then
        echo "Installing Zoom via Flatpak . . ." 
    	flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

    	flatpak --noninteractive install flathub us.zoom.Zoom
    else
        echo "Program Zoom is already installed via Flatpak." 
    fi
else
    echo "Program Zoom is already installed." 
fi


exit

