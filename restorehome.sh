#!/bin/bash

# Home path where files are to be restored to
dcf="/home/rick"


case "$1" in
	"--help" | "-h")
        	echo ""
	    	echo "Format for restorehome.sh . . . "
	    	echo ""
	    	echo "restorehome.sh (source file: all=everyfile)"
	    	echo ""
	        echo ""
	    	echo "Examples"
	        echo ""
		echo "restorehome.sh all"
		echo "--Restores the entire saved home directory back to $dcf"
		echo ""
		echo "restorehome.sh /PDF/Invoices.pdf"
		echo "--Restores the file 'Invoices.pdf' back to its home directory at"
		echo "  $dcf/PDF"
		echo ""
		echo "restorehome.sh /PDF"
		echo "--Restores the /PDF directory & its contents and saves it in"
		echo "   the directory $dcf/PDF"
	   	exit 0
		;;
	"")
		echo "Incorrect syntax, please check --help or -h for more info"
		;;
	"all")
		scf="/media/rick/SSD Storage/"
		;;
	*)
		scf="/media/rick/SSD Storage$1"
		;;
esac


rsync -avW --progress  "$scf" "$dcf"

