#!/bin/bash

# Toggle touchpad script

luck=$(xinput list --long 'SynPS/2 Synaptics TouchPad' | grep 'device is disabled')
if [ -n "$luck" ]; then
    xinput enable "SynPS/2 Synaptics TouchPad"
    notify-send "Enabled" "The touchpad is enabled."
else
    xinput disable "SynPS/2 Synaptics TouchPad"
    notify-send "Disabled" "The touchpad is disabled."
fi

