#!/bin/bash


# Install candy icon set - This tarball can be downloaded at:
#	https://www.gnome-look.org/p/1305251/
#	Click on the "Download" button on the right-top of the window
#	then click on the tar file to download it. Put the file
#	in the directory -- ~/Downloads/candy-icons -- otherwise, the script
#	will not complete the install.

if [ -f $HOME/Downloads/candy-icons/candy-icons.tar.xz ]; then

	cd $HOME/Downloads/candy-icons
	ls $HOME/.icons | grep candy
	if [ $? -gt 0 ]; then
		echo "Installing Candy Icons . . ."
		if [ ! -d $HOME/.icons ]; then
			mkdir $HOME/.icons
		fi
		tar -xf ./candy-icons.tar.xz --one-top-level=$HOME/.icons
	else
		echo "Candy Icons are already installed."
	fi

	cd $HOME

else
	echo "Skipping installing candy icons due to can't find the 'tar' file."
	echo "You need to download the tarball from the following website:"
	echo "https://www.gnome-look.org/p/1305251/"
	echo "And place it in your Home directory at:"
	echo '/home/<user>/Downloads/candy-icons'
	sleep 10
fi

exit

